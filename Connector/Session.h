#ifndef _AURI_SESSION_H_
#define _AURI_SESSION_H_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WinInet.h>
#include <stdlib.h>
#pragma comment(lib,"WinInet")
typedef struct _SESSION
{
	LPVOID Session;
	LPVOID Connection;
	LPVOID Request;
}SESSION,*PSESSION;

	BOOLEAN CreateSession(PSESSION Session);
	BOOLEAN EstablishConnection(PSESSION Session, LPSTR Server, INTERNET_PORT Port);
	BOOLEAN CreateRequest(PSESSION Session, LPSTR RequestType, LPSTR RequestPath, LPSTR Headers, LPSTR RequestQuery, BOOLEAN UseSecureConnection);
	BOOLEAN ReadRequestHeaders(PSESSION Session, PCHAR* Buffer, PULONG Size);
	BOOLEAN ReadRequest(PSESSION Session, PCHAR* Buffer, PULONG Size);
	VOID DestroyRequest(PSESSION Session);
	VOID DestroyConnection(PSESSION Session);
	VOID DestroySession(PSESSION Session);


#endif