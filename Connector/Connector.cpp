

#include "Session.h"


#pragma comment(lib,"Http.lib")
int main()
{
	SESSION Google = { 0 };
	if (CreateSession(&Google))
	{
		if (EstablishConnection(&Google, "Google.com", INTERNET_DEFAULT_HTTPS_PORT))
		{
			if (CreateRequest(&Google, "GET", "/", NULL, NULL, TRUE))
			{
				char* Request = NULL;
				ULONG RequestSize = 0;
				if (ReadRequestHeaders(&Google, &Request, &RequestSize))
				{
					Request[RequestSize] = 0;
					free(Request);
					Request = NULL;
					RequestSize = 0;
				}
				if (ReadRequest(&Google, &Request, &RequestSize))
				{
					Request[RequestSize-1] = 0;
					free(Request);
					Request = NULL;
					RequestSize = 0;
				}
				DestroyRequest(&Google);
			}
			
			DestroyConnection(&Google);
		}
		DestroySession(&Google);
	}

	return 0;
}